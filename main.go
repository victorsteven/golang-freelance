package main

import (
	"fmt"
	"io"
	"math/rand"
	"reflect"
	"sort"
	"strings"
)


type t1 struct {
	team string
	name string
	time string
	event string
}

type t2 struct {
	team string
	name string
	time string
	event string
}

func main() {
	//This implements the io.Reader interface
	var r io.Reader
	r = strings.NewReader("Read this")
	mine(r)

	//This will not implement tbe io.Reader interface
	mine("Hello sir")

	var y interface{}
	y = "hello"
	var z interface{}
	z = "hello"

	//	using the reflect package:
	ok := reflect.DeepEqual(y, z)
	if !ok {
		fmt.Println("the two are never equal")
	} else {
		fmt.Println("The two are of the same interface type")
	}

	fmt.Println("this is for the greeting")
	fmt.Printf("%s, World", greeting())


	//e1 := []t1{
	//	{ "can", "ZAlice", "23+2", "hello"},
	//	{"Ore", "David", "200", "Hi"},
	//}
	//
	//e2 := []t2{
	//	{"gao", "Alice", "23", "hello"},
	//	{"kele","David", "2", "Hi"},
	//}

	//getEvents(e1, e2)

}

func mine(x interface{}) {
	r, ok := x.(io.Reader) //check if x is of type io.Reader
	if !ok {
		fmt.Println("x is not of type io.Reader interface")
	} else {
		fmt.Println("We have the interface implemented", r)
	}
}

func greeting() string {
	g := []string{"Hello", "Howdy", "Hi", "Good morning"}
	return g[rand.Intn(len(g))]
}



//func getEvents(team1 string, team2 string, events1 []t1, events2 []t2) {
//	sort.SliceStable(events1, func(i, j int) bool {
//		return events1[i].name < events1[j].name
//	})
//
//	sort.SliceStable(events2, func(i, j int) bool {
//		return events2[i].name < events2[j].name
//	})
//
//	//team1 = events1
//}

func getEvents(team1 string, team2 string, events1 []t1, events2 []t2) {
	sort.SliceStable(events1, func(i, j int) bool {
		return events1[i].time < events1[j].time
	})

	sort.SliceStable(events2, func(i, j int) bool {
		return events2[i].time < events2[j].time
	})

	fmt.Println("this is event 1: ", events1)
	fmt.Println("this is event 2: ", events2)
}



//e1 := []struct {
//Name string
//Age  int
//}{
//{"Alice", 23},
//{"David", 2},
//{"Eve", 2},
//{"Bob", 25},
//}
