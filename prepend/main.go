package main

import "fmt"

func main() {
	givenArr := []string{"Jon", "Mike", "Peter"}

	fmt.Println(prepend(givenArr))
}
//This function add hello to all the string in the array
func prepend(givenArray []string) []string {
	for i, f := range givenArray {
		givenArray[i] = "hello " + f
	}
	return givenArray
}