package main

import (
	"errors"
	"fmt"
	"os"
)

func main() {

	err := errors.New("access denied")
	fmt.Println(err)
	fmt.Printf("type of err: %T\n", err)

	file, err := os.Open("mango.txt")
	if err != nil {
		fmt.Println("File opening error ", err)
		return
	}
	fmt.Println("Contents of file: ", file)
}
