package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
)

func main() {

	//mystring := strings.Map()
	//var sen = strings.Builder{}

	//var mine int
	//var err error
	//const ErrPerm = "unauthorized"
	//errValue := fmt.Errorf("access denied: %v", ErrPerm)
	//fmt.Println(errValue)

	//err := errors.New("access denied")
	//fmt.Println(err)
	//fmt.Printf("type of err: %T\n", err)

	//errString := err.Error()
	//fmt.Println("Another error",  errString)
	//fmt.Printf("type of err: %T\n", errString)

	usingError()
	usingWrap()
	//wrapExample()
	readingFile()
}

//func wrapExample(){
//	errm := errors.New("access denied")
//	errString := errm.Error()
//
//	err := errors.Unwrap(errString)
//	fmt.Println(err)
//}

func usingWrap() {
	appError := AppError{
		Query: "",
		Err:   errors.New("this is an app error"),
	}
	fmt.Printf("%T\n", appError.Unwrap())
}

func usingError() {
	customErr := errMessage{
		Message: "not found",
		Err: errors.New("we didnt find resource"),
	}
	fmt.Println(customErr.Error())
}



type ModelMissingError struct {
	msg string
	err error
}
func (m ModelMissingError) Error() string {
	return "this is a model error"
}


func sqlError(err error) string {
	var myErr ModelMissingError
	if errors.Is(err, myErr.err) {
		return err.Error()
	}
	return "not a mysql error"
}


//func errorType(err error) string {
//	var mysqlErr *mysql.MySQLError
//	if errors.As(err, &mysqlErr) {
//		return err.Error()
//	}
//	return "not a mysql error"
//}

//func sqlError1(err error) string {
//
//	sqlErr, ok := err.(ModelMissingError)
//		if ok {
//			return sqlErr.Error()
//		}
//	return "not a mysql error"
//}

func readingFile()  {
	file, err := os.Open("myfile.txt")
	defer file.Close()
	if err != nil {
		fmt.Println("this is the file error", err)
		return
	}
	reader := bufio.NewReader(file)
	var line string
	for {
		line, err = reader.ReadString('\n')
		fmt.Printf(" > Read %d characters\n", len(line))
		if err != nil {
			break
		}
	}
	if errors.Is(err, io.EOF)  {
		fmt.Println("Finished reading the file")
		return
	}
}



type errMessage struct {
	Message string
	Err error
}
//Since the ErrMessage struct defines the Error() method, it //implements the error interface
func (err *errMessage) Error() string {
	return err.Message + ", " + err.Err.Error()
}


type AppError struct {
	Query string
	Err   error
}
func (e *AppError) Unwrap() error {
	return e.Err
}



