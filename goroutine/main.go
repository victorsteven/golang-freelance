package main

import (
	"fmt"
	"time"
)

func main() {
	//go hello() //this will not, because unlike functions, the control will not wait for the goroutine to finish executing
	//hello() //The function output will be printed with this

	//To make the hello gorutine to run, we will delay the program for one second:
	go hello()
	time.Sleep(1 * time.Millisecond)
	fmt.Println("main function")
}

func hello() {
	fmt.Println("Hello world")
}
