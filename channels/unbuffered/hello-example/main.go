package main

import "fmt"


func hello(done chan bool) {
	fmt.Println("Hello goroutine")
	done <- true //send_write true to the channel (we are writing to the channel)
}

func main() {
	done := make(chan bool)
	go hello(done)
	//c  := <-done  //we are reading from the channel
	//fmt.Println("this is the reading output", c)
	//The above can simply be done like this:
	<-done
	fmt.Println("this is from the main routine")
}



