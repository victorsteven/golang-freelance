package main

import "fmt"

func producer(chnl chan int) {
	for i := 0; i < 10; i++ {
		chnl <- i
	}
	close(chnl)
}

func main() {
	ch := make(chan int)
	go producer(ch)
	//for {
	//	v, ok := <-ch
	//	if ok == false {
	//		fmt.Println("this is the end of the channel")
	//		break
	//	}
	//	fmt.Println("Received ", v, ok)
	//}

	//	using for range automatically exit the program when the channel is close
	for v := range ch {
		fmt.Println("Received from the range: ", v)
	}

	number := 589
	sqrch := make(chan int)
	cubech := make(chan int)

	go calSquares(number, sqrch)
	go calCubes(number, cubech)
	squares, cubes := <-sqrch, <-cubech
	fmt.Println("FInal output", squares + cubes)

}

func digits(num int, dchnl chan int) {
	for num != 0 {
		digit := num % 10
		dchnl <- digit
		num /= 10
	}
	close(dchnl)
}
func calSquares(num int, sqch chan int) {
	sum := 0
	dch := make(chan int)
	go digits(num, dch)
	//the channel is listened to until it is closed
	for digit := range dch {
		sum += digit * digit
	}
	sqch <- sum
}

func calCubes(num int, cubech chan int) {
	sum := 0
	dch := make(chan int)
	go digits(num, dch)
	//the channel is listened to until it is closed
	for digit := range dch {
		sum += digit * digit * digit
	}
	cubech <- sum
}


