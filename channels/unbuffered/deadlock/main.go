package main

func main() {
	//this will result in a deadlock
	ch := make(chan int)
	ch <- 5
}

