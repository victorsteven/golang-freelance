package main

import "fmt"

func sendData(sendch chan<- int) {
	sendch <- 10
}
func main() {
	//sendch := make(chan<- int)
	//go sendData(sendch)

	//Trying to get a valid from this send_write channel will output an error
	//main.go:11: invalid operation: <-sendch (receive from send_write-only type chan<- int)
	//fmt.Println(<-sendch)

//	To help us read_receive from this channel,we will use type conversion, ie we will create a bidirectional channel
	chanch := make(chan int)
	go sendData(chanch)
	fmt.Println(<-chanch)

}
