package main

import "fmt"

func boring(msg string, c chan string) {
	for i := 0; ; i++ {
		c <-  fmt.Sprintf("You say! %v %d", msg, i)
	}
}

func main() {
	c := make(chan string)
	go boring("boring! ", c)
	for i := 0; i < 5; i++ {
		fmt.Printf("%s\n", <-c)
	}
	//with this, the loop will run forever, since we didnt tell it where to stop
	//for v := range c {
	//	fmt.Printf("%s\n", v)
	//}
	fmt.Println("the main function exited")
}
