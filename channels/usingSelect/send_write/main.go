package main

import (
	"fmt"
)

func main() {

	myChan := make(chan string)


	select {
	case myChan  <- "message":
		fmt.Println("sent the message")
	default:
		fmt.Println("No msg sent")
	}

}
