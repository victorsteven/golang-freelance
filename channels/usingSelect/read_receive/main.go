package main

import (
	"fmt"
	"time"
)

func main() {

	myChan := make(chan string)

	go func() {
		myChan <- "Message!"
	}()
	<-time.After(time.Second * 1) //this is important before the select below to give our goroutine the opportunity to run.

	//This select achieves non blocking read_receive from a channel
	select {
	case msg := <-myChan:
		fmt.Println(msg)
	default:
		fmt.Println("No msg")
	}
}
