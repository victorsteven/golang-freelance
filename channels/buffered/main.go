package main

import (
	"fmt"
	"time"
)

func main() {
	messages := make(chan string, 2)
	messages <- "buffered"
	messages <- "channel"
	//messages <- "channel again" //writing a third value will result to a deadlock


	//fmt.Println(<-messages)
	//fmt.Println(<-messages)

	//OR
	//Note that this loop must not be infinite, it must have a max of based on the channel capacity, else it will result in deadlock
	for i := 0; i < 2; i++ {
		fmt.Println(<-messages)
	}
	//Below will result in a deadlock
	//for {
	//	fmt.Println(<-messages)
	//}

	ch := make(chan int, 2)
	go write(ch)
	time.Sleep(1 * time.Second)
	for v := range ch {
		fmt.Println("read_receive value", v, "from ch")
		time.Sleep(2 * time.Second)
	}

	caplen()
}

func write(ch chan int) {
	for i := 0; i < 5; i++ {
		ch <- i
		fmt.Println("successfully wrote ", i, "to ch")
	}
	close(ch)
}

func caplen() {
	ch := make(chan string, 3)
	ch <- "hello"
	ch <- "mike"
	fmt.Println("capacity is ", cap(ch))
	fmt.Println("length is ", len(ch))

	fmt.Println("read_receive value", <-ch)
	fmt.Println("new length is", len(ch))
}

