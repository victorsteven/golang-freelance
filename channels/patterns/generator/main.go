package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	//c := boring("boring!")
	//for i := 0; i < 5; i++ {
	//	fmt.Printf("You say: %v\n", <-c)
	//}
	//fmt.Println("You're boring, am leaving!")

	//ans := fanIn(boring("hello"), boring("hi"))
	//for i := 0; i < 5; i++ {
	//	fmt.Printf("You say: %v\n", <-ans)
	//}
	//fmt.Println("You're boring, am leaving! select")

	c := boring("joe")

	//Time out the whole loop
	//timeout := time.After(5 * time.Second)
	//for {
	//	select {
	//		case s := <-c:
	//			fmt.Println(s)
	//		case <- timeout:
	//			fmt.Println("You talk too much ")
	//			return
	//	}
	//}

	//Timeout each message
	for {
		select {
		case s := <-c:
			fmt.Println(s)
		case <- time.After(1 * time.Second):
			fmt.Println("You are too slow")
			return
		}
	}
}

//this function returns receive-only channel of strings
func boring(msg string) <-chan string {
	c := make(chan string)
	go func() { //we launch the goroutine from inside the function
		for i := 0;  ; i++ {
			c <- fmt.Sprintf("%s %d", msg, i)
			time.Sleep(time.Duration(rand.Intn(1e3)) * time.Millisecond)
		}
	}()
	return c //Return the channel to the caller
}

func fanIn(input1, input2 <-chan string) <-chan string {
	c := make(chan string)
	go func() {
		for {
			select {
				case s := <-input1: c <- s
				case s := <-input2: c <- s
			}
		}
	}()
	return c
}
