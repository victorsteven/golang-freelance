package main

import (
	"fmt"
	"time"
)

func main() {
	theMine := []string{"rock", "ore", "ore", "rock", "ore"}
	oreChan := make(chan string)
	minedChan := make(chan string)

	////The done channel
	//doneChan := make(chan string)
	//go func() {
	////		Do some work
	//doneChan <- "I'm all done!"
	//}()
	//<-doneChan //block until go routine signals work is done

	//	Finder
	go func(mine []string) {
		for _, item := range mine {
			if item == "ore" {
				oreChan <- item //send_write
			}
		}
	}(theMine)

	//Ore Breaker
	//go func() {
	//	for i := 0; i < 3; i++ {
	//		foundOre := <-oreChan //read_receive from oreChan
	//		fmt.Println("From Finder: ", foundOre)
	//		minedChan <- foundOre //send_write to the minedOreChan
	//	}
	//}()
	//OR
	go func() {
		for foundOre := range oreChan {
			fmt.Println("From Finder: ", foundOre)
			minedChan <- foundOre //send_write to the minedOreChan
		}
	}()
	//Smelter
	//go func() {
	//	for i := 0; i < 3; i++ {
	//		minedOre := <-minedChan //read_receive from the minedChan
	//		fmt.Println("From miner: ", minedOre)
	//		fmt.Println("From Smelter: Ore is smelted")
	//	}
	//}()
	//OR
	go func() {
		for minedOre := range  minedChan {
			fmt.Println("From miner: ", minedOre)
			fmt.Println("From Smelter: Ore is smelted")
		}
	}()
	<-time.After(time.Second * 5)



	//bufferedChan := make(chan string, 3)
	//go func() {
	//	bufferedChan <- "first"
	//	fmt.Println("Sent 1st")
	//	bufferedChan <- "second"
	//	fmt.Println("Sent 2nd")
	//	bufferedChan <- "third"
	//	fmt.Println("Sent 3rd")
	//}()
	//<-time.After(time.Second * 1)
	//go func() {
	//	firstRead := <- bufferedChan
	//	fmt.Println("Receiving..")
	//	fmt.Println(firstRead)
	//	secondRead := <- bufferedChan
	//	fmt.Println(secondRead)
	//	thirdRead := <- bufferedChan
	//	fmt.Println(thirdRead)
	//}()
	//<-time.After(time.Second * 1)
}
